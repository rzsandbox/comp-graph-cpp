
#include <any>
#include <cassert>
#include <iostream>
#include <sstream>
#include <thread>
#include <type_traits>
#include <unordered_map>
#include <vector>
#include <fmt/core.h>

namespace rz::graph
{

using f64 = double;
struct node;

struct base_io_data
{
  virtual std::any any_ptr() = 0;

  virtual void replace(std::any&&) = 0;

  virtual std::string to_string() const = 0;

  friend std::ostream& operator<<(std::ostream& os, const base_io_data& v)
  {
    return os << v.to_string();
  }
};

template<typename T>
struct io_data : base_io_data
{
  io_data() = default;

  [[maybe_unused]] io_data(T data)
      : data_(std::move(data))
  {
  }

  io_data(const io_data&) = delete;

  io_data& operator=(const io_data&) = delete;

  void replace(std::any&& any) override
  {
    data_ = std::any_cast<T>(any);
  }

  std::any any_ptr() override
  {
    return &data_;
  }

  T& value()
  {
    return data_;
  }

  [[nodiscard]] std::string to_string() const override
  {
    auto* data_p = data_ptr();
    if constexpr(std::is_same_v<double*, decltype(data_p)>) {
      return fmt::format("{}", *data_p);
    }
    else {
      return std::to_string(*data_p);
    }
  }

  T data_{};

  constexpr auto* data_ptr() const
  {
    if constexpr(std::is_pointer<T>::value) {
      return data_;
    }
    else {
      return &data_;
    }
  }
};

struct io_attributes
{
  explicit io_attributes(node* node)
      : node_(node)
  {
  }

  void add(std::string name, base_io_data* input)
  {
    data_.insert_or_assign(std::move(name), input);
  }

  void rename(const std::string& old_name, const std::string& new_name)
  {
    auto entry = data_.extract(old_name);
    if(!entry.empty()) {
      entry.key() = new_name;
      data_.insert(std::move(entry));
    }
  }

  virtual void on_declare_inputs(){};

  virtual void on_declare_outputs(){};

  node* node_;
  std::unordered_map<std::string, base_io_data*> data_;
};

struct init_info
{
};

struct node
{
  virtual ~node() = default;

  node(init_info* init_info, io_attributes* inputs, io_attributes* outputs)
      : init_info_p_(init_info)
      , inputs_p_(inputs)
      , outputs_p_(outputs)
  {
  }

  void evaluate()
  {
    on_evaluate();
  }

  void declare_attributes()
  {
    if(inputs_p_) {
      inputs_p_->on_declare_inputs();
    }
    if(outputs_p_) {
      outputs_p_->on_declare_outputs();
    }
    on_declare_attributes();
  }

  virtual void on_evaluate() = 0;

  virtual void on_declare_attributes() {}

  init_info* init_info_p_;
  io_attributes* inputs_p_;
  io_attributes* outputs_p_;
};

struct graph
{
  graph(std::vector<std::unique_ptr<node>> nodes)
      : nodes_(std::move(nodes))
  {
  }

  void evaluate()
  {
    for(auto& n : nodes_) {
      n->evaluate();
    }
  }

  std::vector<std::unique_ptr<node>> nodes_;
};

struct graph_builder
{
  void add(const std::string& name, std::unique_ptr<node> node)
  {
    node->declare_attributes();

    auto* input_attrs = node->inputs_p_;
    auto* output_attrs = node->outputs_p_;

    if(input_attrs) {
      for(auto& [k, v] : input_attrs->data_) {
        inputs_[k].push_back(v);
      }
    }

    if(output_attrs) {
      for(auto [k, v] : output_attrs->data_) {
        static const std::string dot = ".";
        std::string key_str;
        key_str.append(name);
        key_str.append(".");
        key_str.append(k);
        outputs_[key_str] = v;
      }
    }

    nodes_.push_back(std::move(node));
  }

  graph build()
  {
    for(auto& [k_in, v_in] : inputs_) {
      assert(outputs_.contains(k_in));
      for(auto& io_in : v_in) {
        auto io_out = outputs_[k_in]->any_ptr();
        io_in->replace(std::move(io_out));
      }
    }
    return {std::move(nodes_)};
  }

private:
  std::vector<std::unique_ptr<node>> nodes_;
  std::unordered_map<std::string, std::vector<base_io_data*>> inputs_;
  std::unordered_map<std::string, base_io_data*> outputs_;
};

struct computational_node : node
{
  computational_node(init_info* init_info, io_attributes* inputs, io_attributes* outputs)
      : node(init_info, inputs, outputs)
  {
  }

private:
};
} // namespace rz::graph