
#include <rz/comp_graph.hpp>

using namespace rz::graph;

/// user stuff

struct node1_outputs : io_attributes
{
  explicit node1_outputs(node* node)
      : io_attributes(node)
  {
  }

  void on_declare_outputs() override
  {
    auto* outputs = node_->outputs_p_;
    outputs->add("x", &output1);
    outputs->add("y", &output2);
  }

  io_data<double> output1{0};
  io_data<double> output2{0};
};

struct node1 : computational_node
{
  node1()
      : computational_node(nullptr, nullptr, &outputs_)
      , outputs_(this)
  {
  }

  void on_evaluate() override
  {
    outputs_.output1.value() += 1.0;
    outputs_.output2.value() += 2.0;
  }

private:
  node1_outputs outputs_;
};

/// printers

struct printer_init_info : init_info
{
  std::string input_name_;
};

template<typename T>
struct printer_inputs : io_attributes
{
  explicit printer_inputs(node* node)
      : io_attributes(node)
  {
  }

  void on_declare_inputs() override
  {
    add("input", &input_);
  }

  io_data<T*> input_{};
};

template<typename T>
struct printer : computational_node
{
  printer(std::string input_name, std::string print_prefix)
      : computational_node(&init_info_, &inputs_, nullptr)
      , init_info_{{}, std::move(input_name)}
      , inputs_(this)
      , print_prefix_("Printing: " + std::move(print_prefix) + ", input: ")
  {
  }

  void on_declare_attributes() override
  {
    inputs_.rename("input", init_info_.input_name_);
  }

  void on_evaluate() override
  {
    print_value_ = inputs_.input_.to_string();
    fputs(print_prefix_.c_str(), stdout);
    puts(print_value_.c_str());
    // printf("%s%s\n", print_prefix_.c_str(), print_value_.c_str());
  }

  static std::unique_ptr<printer<T>> make(std::string input_name, std::string print_prefix)
  {
    return std::make_unique<printer<T>>(std::move(input_name), std::move(print_prefix));
  }

  printer_init_info init_info_{};
  printer_inputs<T> inputs_;
  std::string print_prefix_;
  std::string print_value_;
};

/// multiplier

struct multiplier_init_info : init_info
{
  std::string input1_name;
  std::string input2_name;
};

struct multiplier_inputs : io_attributes
{
  explicit multiplier_inputs(node* node)
      : io_attributes(node)
  {
  }

  void on_declare_inputs() override
  {
    add("input1", &input1);
    add("input2", &input2);
  }

  io_data<double*> input1{0};
  io_data<double*> input2{0};
};

struct multiplier_outputs : io_attributes
{
  explicit multiplier_outputs(node* node)
      : io_attributes(node)
  {
  }

  void on_declare_outputs() override
  {
    add("product", &product);
  }

  io_data<double> product{0};
};

struct multiplier : computational_node
{
  explicit multiplier(multiplier_init_info init_info)
      : computational_node(&init_info_, &inputs_, &outputs_)
      , init_info_(std::move(init_info))
      , inputs_(this)
      , outputs_(this)
  {
  }

  void on_declare_attributes() override
  {
    inputs_.rename("input1", init_info_.input1_name);
    inputs_.rename("input2", init_info_.input2_name);
  }

  void on_evaluate() override
  {
    outputs_.product.data_ = (*inputs_.input1.data_) * (*inputs_.input2.data_);
  }

  static std::unique_ptr<multiplier> make(std::string input_name, std::string print_prefix)
  {
    return std::make_unique<multiplier>(multiplier_init_info{{}, std::move(input_name), std::move(print_prefix)});
  }

private:
  multiplier_init_info init_info_;
  multiplier_inputs inputs_;
  multiplier_outputs outputs_;
};

int main()
{
  using namespace rz::graph;
  graph_builder builder;

  builder.add("start", std::make_unique<node1>());

  builder.add("print_x", printer<f64>::make("start.x", "x"));
  builder.add("print_y", printer<f64>::make("start.y", "y"));

  builder.add("product", multiplier::make("start.x", "start.y"));
  builder.add("print_product", printer<f64>::make("product.product", "product"));
  auto graph = builder.build();

  for(int i = 0; i < 10000000; i++) {
    graph.evaluate();
    graph.evaluate();
    graph.evaluate();
  }
}
